const express = require('express');
const cors = require('cors')
const bodyParser = require('body-parser');
const birds = require('./routes/birds');
const book = require('./routes/book');
const users = require('./routes/users');
const main = require('./routes/main');
const example = require('./routes/example');
const { Auth } = require('./shared/helper/auth');
const image = require('./routes/image');
const email = require('./routes/email');
const excel = require('./routes/excel');

const createServer = () => {
    const app = express();

    app.use(cors());
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(bodyParser.json());
    app.use(Auth.initialize());

    // custom route
    app.use('/', main);
    app.use('/birds', birds);
    app.use('/book', book);
    app.use('/users', Auth.authenticate(), users);
    app.use('/example', example);
    app.use('/image', image);
    app.use('/email', email);
    app.use('/excel', excel);

    // middleware
    app.options('*', cors());

    return app;
}

module.exports = createServer;