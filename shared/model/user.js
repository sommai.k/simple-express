const { DataTypes } = require('sequelize');
const sequelize = require('../helper/sequelize');

const User = sequelize.define('User', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        field: 'user_id'
    },
    name: {
        type: DataTypes.STRING(150),
        field: 'user_name',
        allowNull: false,
    }
}, { tableName: 'sc_user' });

module.exports = User;