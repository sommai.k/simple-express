const { createLogger, transports, format } = require("winston");

class Logger {
    constructor(name) {
        this.logger = createLogger({
            level: 'info',
            format: format.combine(
                format.timestamp({
                    format: 'YYYY-MM-DD HH:mm:ss'
                }),
                format.json()
            ),
            defaultMeta: { service: name },
            transports: [
                new transports.File({ filename: 'error.log', level: 'error' }),
                new transports.File({ filename: 'combined.log', level: 'debug' }),
            ],
        });
    }
    info = (str) => {
        this.logger.info(str);
    };

    debug = (str) => {
        this.logger.debug(str);
    };

    error = (str) => {
        this.logger.error(str);
    }
}

exports.Logger = Logger;