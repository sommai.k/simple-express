const { Sequelize } = require('sequelize');
const sequelize = new Sequelize(
    process.env.DB_URL ?? "postgres://usr:12345678@postgres:5432/pg_db"
);

module.exports = sequelize;