const { createClient } = require('redis');

const redisClient = createClient({
    url: 'redis://redis:6379'
});

exports.redisClient = redisClient;