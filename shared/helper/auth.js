const passport = require('passport');
const passportJwt = require('passport-jwt');
const Strategy = passportJwt.Strategy;

const secretKey = process.env.JWT_SECRET ?? 'super-secret-key';

class Auth {
    constructor() {
        let strategy = new Strategy({
            secretOrKey: secretKey,
            jwtFromRequest: passportJwt.ExtractJwt.fromAuthHeaderAsBearerToken(),
        }, (payload, done) => {
            const user = payload;
            if (user) {
                return done(null, user);
            } else {
                return done(new Error("User not found !!!", null));
            }
        });
        passport.use(strategy);
    }

    initialize() {
        return passport.initialize();
    }

    authenticate() {
        return passport.authenticate('jwt', { session: false });
    }
}

exports.secretKey = secretKey;
exports.Auth = new Auth();
