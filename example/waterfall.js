const async = require('async');
const fs = require('fs');

const cb1 = (callback) => {
    callback(null, 'one', 'two');
}

const cb2 = (a, b, callback) => {
    fs.readFile('message.txtxx', 'utf8', (err, data) => {
        if (err) {
            callback(err.message);
        } else {
            callback(null, data)
        }
    });
}

const cb3 = (c, callback) => {
    console.log('cb3 -->', c);
    callback(null, 'done');
}

const result = (err, res) => {
    if (err) {
        console.log('result', err);
    } else {
        console.log(res);
    }
}

async.waterfall([cb1, cb2, cb3], result);