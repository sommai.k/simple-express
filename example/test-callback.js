const fs = require('fs');
const example = require('./example');

const helloCallback = (greeting) => {
    fs.readFile('message.txt', 'utf8', (err, data) => {
        if (err) {
            greeting('Readfile', 'Error', 'No data');
        } else {
            greeting('Hello', 'World', data);
        }
    });
}

helloCallback((h, w, d) => {
    console.log(h, w, d);
});

example.helloWorldCallback((h, w) => {
    console.log(h, w);
});