const fileSys = require('fs');

const readHandler = (err, data) => {
    if (err) {
        console.log(err);
    } else {
        console.log(data);
    }
}

const watchHander = (curr, prev) => {
    fileSys.readFile('message.txt', 'utf8', readHandler)
}

fileSys.watchFile('message.txt', watchHander);