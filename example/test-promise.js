const _prom = (age) => {
    return new Promise((resolve, reject) => {
        if (age >= 18) {
            resolve("Hello");
        } else {
            reject("age < 18");
        }
    });
}

_prom(20).then((value) => {
    console.log(value);
}).catch((reason) => {
    console.log(reason);
});

_prom(14).then((value) => {
    console.log(value);
}).catch((reason) => {
    console.log(reason);
});

const callFunc = async () => {
    try {
        const d = await _prom(25);
        console.log('call func', d);
    } catch (ex) {
        console.log(ex);
    }

    try {
        const e = await _prom(13);
        console.log('call func err', e);
    } catch (ex) {
        console.log('error await', ex);
    }
}

callFunc();