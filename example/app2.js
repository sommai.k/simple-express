const http = require('http');

const server = http.createServer().listen(8000);

const handler = (req, res) => {
    res.writeHead(200);
    res.end("New server");
};

server.on('request', handler);

console.log('Servers start on port 8000 !!!!');