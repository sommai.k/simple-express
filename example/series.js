const async = require('async');

const cb1 = (callback) => {
    callback(null, 'One');
}

const cb2 = (callback) => {
    callback(null, 'Two');
}

const cbe = (callback) => {
    callback('Error');
}

const result = (err, results) => {
    if (err) {
        console.log(err, results);
    } else {
        console.log(results);
    }
}

async.series([cb1, cb2, cbe], result);