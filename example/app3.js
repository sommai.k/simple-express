require('./foo');
foo();
foo2();

const superBar = require('./bar');
superBar();

const fiz = require('./fiz');
fiz.fiz();
fiz.fiza();

const fiz2 = require('./fiz').fiz;
fiz2();

const buz = require('./buz');
buz.log();

const baz = require('./baz').Baz;
baz.log();

const Doo = require('./doo');
const doo = new Doo();
doo.log();

const Qux = require('./qux').Qux;
const qux = new Qux();
qux.log();

const nux = require('./qux').Nux1;
nux.log();