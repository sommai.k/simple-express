const async = require('async');

const cb1 = (callback) => {
    console.log('One Start');
    setTimeout(() => {
        console.log('One');
        callback(null, 'One');
    }, 200);
    console.log('One End');
}

const cb2 = (callback) => {
    console.log('Two Start');
    const handler = () => {
        console.log('Two');
        callback(null, 'Two');
    };
    setTimeout(handler, 100);
    console.log('Two End');
}

const cbe = (callback) => {
    const handler = () => {
        callback('Error');
    };
    setTimeout(handler, 150);
}

const result = (err, results) => {
    if (err) {
        console.log(err, results);
    } else {
        console.log(results);
    }
}

async.parallel([cb1, cb2, cbe], result);
console.log('End Process');