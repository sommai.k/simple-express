const createServer = require('../../server');
const supertest = require('supertest');


describe('/email', () => {
    const app = createServer();
    jest.mock("nodemailer");
    const nodemailer = require("nodemailer");
    const sendMailMock = jest.fn().mockReturnValue();

    beforeEach(() => {
        sendMailMock.mockClear();
        nodemailer.createTransport.mockClear();
    });


    test.skip("Should send email success", async () => {

        nodemailer.createTransport.mockReturnValue({ "sendMail": (opt, callback) => callback(null, "") });
        const resp = await supertest(app).post('/email').expect(200);
        expect(sendMailMock).toHaveBeenCalled();
    });

});