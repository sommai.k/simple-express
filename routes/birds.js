const router = require('express').Router();

router.get('/about', (req, res) => {
    res.json({
        success: true,
        message: 'About birds'
    })
})

router.get('/', (req, res) => {
    res.json({
        success: true,
        data: [{ code: "", name: "", type: "" }, { code: "", name: "", type: "" }]
    })
})


module.exports = router;