const router = require('express').Router();
const { query } = require('express-validator');
const inputValidate = require('../shared/helper/inputValidate');

const getValidate = [
    query("id").notEmpty().isNumeric(),
    inputValidate
]

const cb0 = (req, res, next) => {
    console.log('CB0');
    next();
}

const cb1 = (req, res, next) => {
    console.log('CB1');
    next();
}

const cb2 = (req, res) => {
    res.json({ success: true, message: "from example" })
}

router.get('/c', getValidate, [cb0, cb1, cb2]);

module.exports = router;