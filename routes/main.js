const router = require('express').Router();
const jwt = require('jwt-simple');
const { secretKey } = require('../shared/helper/auth');
const { body } = require('express-validator');
const inputValidate = require('../shared/helper/inputValidate')

router.get('/', (req, res) => {
    res.json({ success: true });
});

const loginValidate = [
    body('username').trim().notEmpty().isString().isLength({ min: 5, max: 25 }),
    body('password').trim().notEmpty().isString().isLength({ min: 8, max: 25 }),
    inputValidate
]
router.post('/login', loginValidate, (req, res) => {
    console.log(secretKey);
    const token = jwt.encode({ id: 1 }, secretKey);
    res.status(200).json({
        success: true,
        token
    });
});

module.exports = router;