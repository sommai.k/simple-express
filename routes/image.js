const router = require('express').Router();
const multer = require('multer');
const { query } = require('express-validator');
const inputValidate = require('../shared/helper/inputValidate');
const { uploadPath } = require('../shared/helper/parameter');

const upload = multer({
    dest: uploadPath
});

router.post('/', upload.single('file'), (req, res) => {
    console.log(req.file);
    res.status(200).json({
        success: true,
        message: req.file.filename
    })
});

const getValidate = [
    query("file").notEmpty().isString().isLength({ max: 100 }),
    inputValidate
]

router.get('/', getValidate, (req, res) => {
    const { file } = req.query;
    res.download(`${uploadPath}/${file}`, 'test.jpg');
});

module.exports = router;