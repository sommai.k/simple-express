const router = require('express').Router();
const { param, body, query } = require('express-validator');
const inputValidate = require('../shared/helper/inputValidate');
const User = require('../shared/model/user');
const Logger = require('../shared/helper/logger').Logger;
const redisClient = require('../shared/helper/redisClient').redisClient;

const logger = new Logger("users");

const getValidate = [
    param("userId").notEmpty().isNumeric(),
    param("bookId").notEmpty().isNumeric(),
    inputValidate
];

router.get('/:userId/books/:bookId', getValidate, (req, res) => {
    console.log(req.params);
    res.json({ success: true, message: "req.params complete !!!" });
});

router.get('/all', async (req, res) => {
    try {
        const cacheKey = 'users.find.all';
        const ttl = 15;
        const cacheData = await redisClient.get(cacheKey);
        if (cacheData) {
            return res.status(200).json(JSON.parse(cacheData));
        }
        const { rows, count } = await User.findAndCountAll();
        const respData = { success: true, data: rows, total: count };
        await redisClient.setEx(cacheKey, ttl, JSON.stringify(respData));
        res.json(respData);
    } catch (e) {
        logger.error(e);
        res.json({ success: false });
    }
});

const postValidate = [
    body('name').notEmpty().isString().isLength({ max: 150 }),
    inputValidate
];

router.post('/', postValidate, async (req, res) => {
    try {
        logger.debug(req.body);
        const { name } = req.body;
        const resp = await User.create({ name });
        res.status(201).json(resp);
    } catch (e) {
        res.status(200).json({ success: false });
    }
});

const deleteValidate = [
    param("id").notEmpty().isInt(),
    inputValidate
]

router.delete('/:id', deleteValidate, async (req, res) => {
    try {
        const { id } = req.params;
        const reps = await User.destroy({
            where: {
                id
            }
        });
        res.status(200).json({ success: true });
    } catch (e) {
        res.status(200).json({ success: false, message: e.message });
    }
});

const putValidate = [
    param('id')
        .notEmpty().isInt(),
    body('name')
        .notEmpty().isString(),
    inputValidate
];

router.put('/:id', putValidate, async (req, res) => {
    try {
        const { id } = req.params;
        const { name } = req.body;
        const data = await User.update({ name }, {
            where: {
                id
            }
        })
        res.status(200).json({ success: true, data });
    } catch (error) {
        logger.error(error);
        res.status(200).json({ success: false, message: error.message });
    }
});

const findByPageValidate = [
    query("limit").notEmpty().isInt({ max: 50, min: 1 }),
    query("offset").notEmpty().isInt({ min: 0 }),
    inputValidate
];

router.get('/', findByPageValidate, async (req, res) => {
    try {
        const { limit, offset } = req.query;
        const resp = await User.findAndCountAll({ limit, offset });
        const { rows, count } = resp;
        res.status(200).json({
            success: true,
            data: rows,
            total: count
        });
    } catch (e) {
        res.status(200).json({
            success: false,
            message: e.message
        });
    }
})

module.exports = router;