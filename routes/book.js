const express = require('express');
const router = express.Router();
const Logger = require('../shared/helper/logger').Logger;
const { body } = require('express-validator');
const inputValidate = require('../shared/helper/inputValidate');

const logger = new Logger("book");

// Business Logic
router.get('/', (req, res) => {
    res.json({
        success: true,
        errorCode: 200,
        errorMessage: "",
        rowCount: 2,
        data: [{ code: "001", name: "Jame" }, { code: "002", name: "Jim" }]
    });
});

const postValidate = [
    body("code").notEmpty().withMessage("กรุณาระบุ code").isString().withMessage("กรุณาระบุเป็นตัวอักษร"),
    inputValidate
];

router.post('/', postValidate, (req, res) => {
    logger.info(req.body);
    res.json({ success: true });
});

router.put('/', (req, res) => {
    console.log(req.body);
    if (req.body.code) {
        //prepare insert to table
        res.json({ success: true });
    } else {
        res.status(400).end();
    }
});

module.exports = router;