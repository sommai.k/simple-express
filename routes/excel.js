const router = require('express').Router();
const excel = require('excel4node')

router.get('/', (req, res) => {
    const wb = new excel.Workbook();
    const ws = wb.addWorksheet("Sheet 1");
    ws.cell(1, 1).string("Reconcile report").style({ font: { bold: true } });
    wb.write('test.xlsx', () => {
        res.download('test.xlsx');
    });

});

module.exports = router;