const createServer = require('./server');
// const sequelize = require('./shared/helper/sequelize');
// const redisClient = require('./shared/helper/redisClient').redisClient;

const SERVER_PORT = 3000;
const app = createServer();
const server = app.listen(SERVER_PORT, async () => {
    // await sequelize.sync();
    // await redisClient.connect();
    console.log(`Server start on port ${SERVER_PORT}`);
});
