const summary = (arr) => {
    let sum = 0;

    if (arr == null) {
        return sum;
    }

    for (i = 0; i < arr.length; i++) {

        if (typeof arr[i] != "string" || /^[0-9]*$/.test(arr[i])) {
            sum += parseFloat(arr[i]);
        }

    }
    return sum;
}

const formatNumber = (value, decimal) => {
    return value.toLocaleString(undefined, { minimumFractionDigits: decimal, maximumFractionDigits: decimal });
}

const sumWithFormat = (arr, decimal) => {
    try {
        const x = this.summary(arr);
        return this.formatNumber(x, decimal);
    }
    catch {
        return "N/A"
    }
}

exports.summary = summary;
exports.formatNumber = formatNumber;
exports.sumWithFormat = sumWithFormat;