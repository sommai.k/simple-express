const createServer = require('../server');
const supertest = require('supertest');

describe('Rest API Server', () => {
    const app = createServer();

    test('Should show 404', async () => {
        const resp = await supertest(app).get('/dummy').expect(404);
    });

});