const createServer = require('../../server');
const supertest = require('supertest');

describe('/ route', () => {
    const app = createServer();

    test('Go to home path', async () => {
        const resp = await supertest(app).get('/').expect(200);
        expect(resp.body.success).toBe(true);
    });

    test('page login', async () => {
        const resp = await supertest(app)
            .post('/login')
            .send({ username: 'xxxxxxx', password: 'Asssdw12345' })
            .expect(200);
        expect(resp.body.success).toBe(true);
    });

    test('Should show 400 when body empty', async () => {
        await supertest(app)
            .post('/login')
            .expect(400);
    });

});