const createServer = require('../../server');
const supertest = require('supertest');
const User = require('../../shared/model/user');
const { secretKey } = require('../../shared/helper/auth');
const jwt = require('jwt-simple');
const { redisClient } = require('../../shared/helper/redisClient');

describe('/users route', () => {
    const app = createServer();
    let token = '';

    beforeAll(() => {
        token = jwt.encode({ id: 0 }, secretKey);
    });

    beforeEach(() => {
        jest.spyOn(redisClient, 'get').mockResolvedValue(null);
        jest.spyOn(redisClient, 'setEx').mockResolvedValue(null);
    });

    afterEach(() => {

    });

    afterAll(() => {

    });

    test('Should render req.params', async () => {
        const resp = await supertest(app)
            .get('/users/39/books/8989')
            .auth(token, { type: 'bearer' })
            .expect(200);
        expect(resp.body.success).toBe(true);
        expect(resp.body).toHaveProperty('message');
    });

    test('Should show error 400 when userId = A', async () => {
        const resp = await supertest(app)
            .get('/users/A/books/8989')
            .auth(token, { type: 'bearer' })
            .expect(400);
        expect(resp.body).toHaveProperty('errors');
        expect(resp.body.errors).toHaveLength(1);
    });

    test('Should show error 400 when bookId = A', async () => {
        const resp = await supertest(app)
            .get('/users/39/books/A')
            .auth(token, { type: 'bearer' })
            .expect(400);
        expect(resp.body).toHaveProperty('errors');
        expect(resp.body.errors).toHaveLength(1);
    });

    test('Should show error 400 when userId = A, bookId = B', async () => {
        const resp = await supertest(app)
            .get('/users/A/books/B')
            .auth(token, { type: 'bearer' })
            .expect(400);
        expect(resp.body).toHaveProperty('errors');
        expect(resp.body.errors).toHaveLength(2);
    });

    test('Should show error 404 /users/1/books/', async () => {
        const resp = await supertest(app)
            .get('/users/1/books/')
            .auth(token, { type: 'bearer' })
            .expect(404);
    });

    test('Should show user data', async () => {
        const mockData = {
            rows: [{
                id: 99, name: 'supertest'
            }],
            count: 1
        };
        const user = jest.spyOn(User, 'findAndCountAll').mockImplementation(() => mockData);
        const resp = await supertest(app)
            .get('/users/all')
            .auth(token, { type: 'bearer' })
            .expect(200);
        console.log(resp.body);
        expect(resp.body).toHaveProperty('success', true);
        expect(resp.body).toHaveProperty('data');
        expect(resp.body).toHaveProperty('total', 1);
    });

    test('Should show success false', async () => {
        const user = jest.spyOn(User, 'findAndCountAll').mockImplementation(() => { throw new Error() });
        const resp = await supertest(app)
            .get('/users/all')
            .auth(token, { type: 'bearer' })
            .expect(200);
        expect(resp.body).toHaveProperty('success', false);
    });

    test('Should create user success', async () => {
        const user = jest.spyOn(User, 'create').mockImplementation(() => { });
        const resp = await supertest(app)
            .post('/users')
            .auth(token, { type: 'bearer' })
            .send({ name: 'xx' }).expect(201);
    });

    test('Should create user fail', async () => {
        const user = jest.spyOn(User, 'create').mockImplementation(() => { throw new Error() });
        const resp = await supertest(app)
            .post('/users')
            .auth(token, { type: 'bearer' })
            .send({ name: 'xx' }).expect(200);
        expect(resp.body).toHaveProperty('success', false);
    });

    test('Should delete user success', async () => {
        jest.spyOn(User, 'destroy').mockImplementation(() => { });
        const resp = await supertest(app)
            .delete('/users/1')
            .auth(token, { type: 'bearer' })
            .expect(200);
        expect(resp.body).toHaveProperty('success', true);
    });

    test('Should not delete user', async () => {
        jest.spyOn(User, 'destroy').mockImplementation(() => { throw new Error('Sql error') });
        const resp = await supertest(app)
            .delete('/users/1')
            .auth(token, { type: 'bearer' })
            .expect(200);
        expect(resp.body).toHaveProperty('success', false);
        expect(resp.body).toHaveProperty('message', 'Sql error');
    });

    test('Should update user success', async () => {
        jest.spyOn(User, 'update').mockImplementation();
        const resp = await supertest(app)
            .put('/users/1')
            .auth(token, { type: 'bearer' })
            .send({ name: 'xxxx' }).expect(200);
        expect(resp.body).toHaveProperty('success', true);
    });

    test('Should not update user when body empty', async () => {
        jest.spyOn(User, 'update').mockImplementation();
        await supertest(app)
            .put('/users/1')
            .auth(token, { type: 'bearer' })
            .expect(400);
    });

    test('Should not update user when id empty', async () => {
        jest.spyOn(User, 'update').mockImplementation();
        await supertest(app)
            .put('/users')
            .auth(token, { type: 'bearer' })
            .expect(404);
    });

    test('Should not update user', async () => {
        jest.spyOn(User, 'update').mockImplementation(() => { throw new Error("Connection fail") });
        const resp = await supertest(app)
            .put('/users/1')
            .auth(token, { type: 'bearer' })
            .send({ name: 'xxxx' }).expect(200);
        expect(resp.body).toHaveProperty('success', false);
        expect(resp.body).toHaveProperty('message', "Connection fail");
    });

    test('Should fetch 10 record', async () => {
        jest.spyOn(User, 'findAndCountAll').mockImplementation(({ offset, limit }) => {
            expect(offset).toBe('0');
            expect(limit).toBe('10');
            return {
                rows: [],
                count: 0
            }
        });
        const resp = await supertest(app)
            .get('/users?offset=0&limit=10')
            .auth(token, { type: 'bearer' })
            .expect(200);
        expect(resp.body).toHaveProperty('success', true);
        expect(resp.body).toHaveProperty('data');
        expect(resp.body).toHaveProperty('total');
    });

    test('should fetch all record', async () => {
        jest.spyOn(User, 'findAndCountAll').mockImplementation(() => {
            return { rows: [], count: 10 }
        });
        const resp = await supertest(app)
            .get('/users/all')
            .auth(token, { type: 'bearer' })
            .expect(200);
        expect(resp.body).toHaveProperty('success', true);
        expect(resp.body).toHaveProperty('data');
        expect(resp.body).toHaveProperty('total');
    });

    test('Should not fetch any record', async () => {
        await supertest(app)
            .get('/users?offset=0&limit=0')
            .auth(token, { type: 'bearer' })
            .expect(400);
    });

    test('Should fetch all data from redis', async () => {
        const obj = {
            success: true,
            data: [],
            total: 0
        }
        jest.spyOn(redisClient, 'get')
            .mockResolvedValue(JSON.stringify(obj));
        await supertest(app)
            .get('/users/all')
            .auth(token, { type: 'bearer' })
            .expect(200);
    });

    test('Should path / show success false', async () => {
        jest.spyOn(User, 'findAndCountAll').mockImplementation(() => {
            throw new Error("mock error");
        });
        const resp = await supertest(app)
            .get('/users?limit=1&offset=0')
            .auth(token, { type: 'bearer' })
            .expect(200);
        expect(resp.body).toHaveProperty('success', false);
    });

    test('Shoud show error user not found', async () => {
        const emptyToken = jwt.encode("", secretKey);
        await supertest(app)
            .get('/users?limit=1&offset=0')
            .auth(emptyToken, { type: 'bearer' })
            .expect(500);
    });

});