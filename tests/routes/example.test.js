const createServer = require('../../server');
const supertest = require('supertest');

describe('/example route', () => {
    const app = createServer();

    test('Should call middle ware', async () => {
        const resp = await supertest(app).get('/example/c?id=11').expect(200);
        expect(resp.body.success).toBe(true);
        expect(resp.body).toHaveProperty('message');
    });

    test('Should error 400 when no id', async () => {
        const resp = await supertest(app).get('/example/c').expect(400);
    });

});