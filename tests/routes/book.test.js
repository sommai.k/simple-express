const createServer = require('../../server');
const supertest = require('supertest');

describe('/book route', () => {
    const app = createServer();
    // test case

    test('Should get value from /book', async () => {
        const resp = await supertest(app).get('/book').expect(200);
        expect(resp.body.success).toBe(true);
        expect(resp.body.errorCode).toBe(200);
        expect(resp.body.errorMessage).toBe("");
        expect(resp.body.rowCount).toBe(2);
        expect(resp.body.data).toHaveLength(2);
        const obj = resp.body.data[0];
        expect(obj).toHaveProperty('code');
        expect(obj).toHaveProperty('name');
    });

    test('Should create data to /book', async () => {
        const resp = await supertest(app).post('/book').send({ code: "001" }).expect(200);
        expect(resp.body.success).toBe(true);
    });

    test('Should not create data to /book', async () => {
        const resp = await supertest(app).post('/book').expect(400);
        expect(resp.body).toHaveProperty("errors");
        expect(resp.body.errors).toHaveLength(2);
    });

    test('Should show 1 message when code = 1', async () => {
        const resp = await supertest(app).post('/book').send({ code: 1 }).expect(400);
        expect(resp.body).toHaveProperty("errors");
        expect(resp.body.errors).toHaveLength(1);
    });

    test('Should update data to /book', async () => {
        const resp = await supertest(app).put('/book').send({ code: "001" }).expect(200);
        expect(resp.body.success).toBe(true);
    });

    test('Should not update data to /book', async () => {
        const resp = await supertest(app).put('/book').expect(400);
    });

});