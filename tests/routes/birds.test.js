const createServer = require('../../server');
const supertest = require('supertest');

describe('/birds', () => {
    const app = createServer();

    test('Should get bird data from /birds', async () => {
        const resp = await supertest(app).get('/birds').expect(200);
        expect(resp.body.success).toBe(true);
        expect(resp.body.data).toHaveLength(2);
        const obj = resp.body.data[0];
        expect(obj).toHaveProperty('code');
        expect(obj).toHaveProperty('name');
        expect(obj).toHaveProperty('type');
    })

    test('Should get bird about data from /birds/about', async () => {
        const resp = await supertest(app).get('/birds/about').expect(200);
        expect(resp.body.success).toBe(true);
        expect(resp.body.message).toBe("About birds");
    })
});