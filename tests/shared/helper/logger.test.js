const Logger = require('../../../shared/helper/logger').Logger;

describe('Logger', () => {
    const testLog = new Logger('logger.test');

    test('Should call info', () => {
        const info = jest.spyOn(testLog.logger, 'info').mockImplementation((str) => null);
        testLog.info("test");
        expect(info).toHaveBeenCalled();
        expect(info).toBeCalledWith("test");
    });

    test('Should call debug', () => {
        const info = jest.spyOn(testLog.logger, 'debug').mockImplementation((str) => null);
        testLog.debug('debug-code')
        expect(info).toHaveBeenCalled();
        expect(info).toBeCalledWith("debug-code");
    });

    test('Should call error', () => {
        const info = jest.spyOn(testLog.logger, 'error').mockImplementation((str) => {
            expect(str).toBe("error-code");
        });
        testLog.error('error-code')
        expect(info).toHaveBeenCalled();
        expect(info).toBeCalledWith("error-code");
    });
});