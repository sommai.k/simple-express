
const summary = require('../sum').summary;
const formatNumber = require('../sum').formatNumber;
const sumWithFormat = require('../sum').sumWithFormat;
const sum = require('../sum');

describe('Sum test suite', () => {

    test('Should show hello', () => {
        const greeting = 'hello';
        expect(greeting).toEqual('hello');
    })

    test('Should sum 1,2,3 equal 6', () => {
        const s = summary([1, 2, 3]);
        expect(s).toEqual(6);
    })

    test('Should result = 0 when receive null', () => {
        const s = summary(null);
        expect(s).toEqual(0);
    })

    test('Should result = 0 when receive []', () => {
        const s = summary([]);
        expect(s).toEqual(0);
    })

    test('Should result = 0 when receive undefined', () => {
        const s = summary();
        expect(s).toEqual(0);
    })

    test('Should result = 4 when receive [1,"A", 3]', () => {
        const s = summary([1, "A", 3]);
        expect(s).toEqual(4);
    })

    test('Should result = 12 when receive [1, "8", 3]', () => {
        const s = summary([1, "8", 3]);
        expect(s).toEqual(12);
    })

    test('Should result = 12.5 when receive [1.0, "8", 3.5]', () => {
        const s = summary([1.0, "8", 3.5]);
        expect(s).toEqual(12.5);
    })

    test('Should show 1,000.00 when 1000 decimal = 2', () => {
        // const spy = jest.spyOn(sum, 'trimNumber').mockImplementation(() => "10")
        const f = formatNumber(1000, 2);

        // expect(spy).toHaveBeenCalledTimes(1);
        expect(f).toEqual("1,000.00");
    });

    test('Should valid json data from request', () => {
        const data = {
            "success": true,
            "errorCode": 200,
            "errorMessage": "",
            "data": [
                {
                    "TrainingId": "020b274d-54f9-4c1a-8fa2-0cc060d0e5c6",
                    "TrainingNo": "CT24052",
                    "TrainingName": " การพัฒนา Mobile Applications ด้วย Flutter 2",
                    "DepartmentOwnerId": 8,
                    "CourseId": "270d36fb-1655-4ad8-9e4f-8f487cb762b1",
                    "CourseTypeId": 1,
                    "DepartmentOwnerName": "HR (ทรัพยากรบุคคล)",
                    "TrainingDetail": "<p>สิ่งที่คุณจะได้เรียนรู้ 2<br>- ผู้เรียนสามารถเข้าใจ และเขียน ภาษา Dart ได้<br>- ผู้เรียนมีความรู้ ความเข้าใจ และหลักการทำงานของ Flutter<br>- ผู้เรียนสามารถใช้เทคโนโลยีใหม่ๆ ทั้งในส่วนของ Mobile App และ Back-end ได้<br>- ผู้เรียนสามารถเขียน Flutter เชื่อมต่อกับฐานข้อมูล /Backend ได้ (REST APIs/Web Services)</p>",
                    "TrainingStatusId": 1,
                    "TrainingStatusName": "Open Course",
                    "UpdatedBy": "System Test API",
                    "UpdatedDate": "2024-03-12T14:15:55",
                    "CourseTypeName": null
                },
            ],
            "rowCount": 0
        }

        expect(data).toHaveProperty('success', true);
        expect(data).toHaveProperty('errorCode', 200);
        expect(data.data).toHaveLength(1);
    });

    test('Should show 18,987.855 when 18987.8546 decimal = 3', () => {
        // const spy = jest.spyOn(sum, 'trimNumber').mockImplementation(() => "10")
        const f = formatNumber(18987.8546, 3);
        // expect(spy).toHaveBeenCalledTimes(1);
        expect(f).toEqual("18,987.855");
    });

    test('Should show 1,000.00 when [500,300,200] decimal = 2', () => {
        const f = sumWithFormat([500, 300, 200], 2);
        expect(f).toEqual("1,000.00");
    });

    test('Should show N/A when summary return null', () => {
        const spy = jest.spyOn(sum, 'summary').mockImplementation(() => null);
        const f = sumWithFormat([500, 300, 200], 2);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toBeCalledWith([500, 300, 200]);
        expect(f).toEqual("N/A");
    });

});