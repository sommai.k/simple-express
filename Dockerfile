FROM node:20-alpine
ENV uploadPath '/upload'

WORKDIR /app
COPY . .
RUN npm i
RUN npm test

EXPOSE 3000
CMD [ "npm", "start" ]